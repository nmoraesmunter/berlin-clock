package com.inkglobal.techtest;


import com.inkglobal.techtest.converter.TimeUnitConverter;
import org.junit.Assert;
import org.junit.Test;

public class BerlinClockTest {

    BerlinClock cut = new BerlinClock();

    @Test
    public void berlinClockTest() {

        Assert.assertEquals(cut.convert("00", "00", "00"), buildResult("Y", "OOOO", "OOOO", "OOOOOOOOOOO", "OOOO"));
        Assert.assertEquals(cut.convert("13", "17", "01"), buildResult("O", "RROO", "RRRO", "YYROOOOOOOO", "YYOO"));
        Assert.assertEquals(cut.convert("23", "59", "59"), buildResult("O", "RRRR", "RRRO", "YYRYYRYYRYY", "YYYY"));
        Assert.assertEquals(cut.convert("24", "00", "00"), buildResult("Y", "RRRR", "RRRR", "OOOOOOOOOOO", "OOOO"));

    }

    private String buildResult(String... args) {
        StringBuilder builder = new StringBuilder();
        for (String arg : args) {
            builder.append(arg);
            builder.append(TimeUnitConverter.LAMP_GROUP_SEPARATOR);
        }
        return builder.toString();
    }


}
