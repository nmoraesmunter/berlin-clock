package com.inkglobal.techtest.converter;


import org.junit.Assert;
import org.junit.Test;

import java.security.InvalidParameterException;

/**
 * User: nmoraes
 * Date: 12/04/14
 */

public class SecondsConverterTest {


    SecondsConverter cut = new SecondsConverter();

    @Test
    public void oddSeconds() {
        Assert.assertEquals(cut.convert("01"), SecondsConverter.OFF_LAMP);
        Assert.assertEquals(cut.convert("11"), SecondsConverter.OFF_LAMP);

    }

    @Test
    public void evenSeconds() {
        Assert.assertEquals(cut.convert("00"), SecondsConverter.YELLOW_LAMP);
        Assert.assertEquals(cut.convert("10"), SecondsConverter.YELLOW_LAMP);

    }

    @Test(expected = InvalidParameterException.class)
    public void invalidParameter() {
        cut.convert("61");
    }

    @Test(expected = NumberFormatException.class)
    public void invalidNumberFormat() {
        cut.convert("6s");
    }

    @Test(expected = NumberFormatException.class)
    public void empty() {
        cut.convert("");
    }

}
