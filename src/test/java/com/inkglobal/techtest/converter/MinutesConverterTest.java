package com.inkglobal.techtest.converter;

import org.junit.Assert;
import org.junit.Test;

import java.security.InvalidParameterException;

/**
 * User: nmoraes
 * Date: 12/04/14
 */
public class MinutesConverterTest {

    MinutesConverter cut = new MinutesConverter();


    @Test
    public void convertMaxMinutes() {
        Assert.assertEquals(cut.convert("59"), "YYRYYRYYRYY YYYY");
    }

    @Test
    public void convertMinMinutes() {
        Assert.assertEquals(cut.convert("00"), "OOOOOOOOOOO OOOO");
    }

    @Test
    public void convertMinutes() {
        Assert.assertEquals(cut.convert("15"), "YYROOOOOOOO OOOO");
        Assert.assertEquals(cut.convert("27"), "YYRYYOOOOOO YYOO");
        Assert.assertEquals(cut.convert("04"), "OOOOOOOOOOO YYYY");

    }

    @Test(expected = InvalidParameterException.class)
    public void invalidParameter() {
        cut.convert("60");
    }

    @Test(expected = NumberFormatException.class)
    public void invalidNumberFormat() {
        cut.convert("5m");
    }


}
