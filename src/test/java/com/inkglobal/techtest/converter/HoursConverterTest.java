package com.inkglobal.techtest.converter;

import org.junit.Assert;
import org.junit.Test;

import java.security.InvalidParameterException;

/**
 * User: nmoraes
 * Date: 12/04/14
 */

public class HoursConverterTest {


    HoursConverter cut = new HoursConverter();


    @Test
    public void convertMaxHours() {
        Assert.assertEquals(cut.convert("24"), "RRRR RRRR");
    }

    @Test
    public void convertMinHours() {
        Assert.assertEquals(cut.convert("00"), "OOOO OOOO");
    }

    @Test
    public void convertHours() {
        Assert.assertEquals(cut.convert("01"), "OOOO ROOO");
        Assert.assertEquals(cut.convert("05"), "ROOO OOOO");
        Assert.assertEquals(cut.convert("12"), "RROO RROO");

    }


    @Test(expected = InvalidParameterException.class)
    public void invalidParameter() {
        cut.convert("25");
    }

    @Test(expected = NumberFormatException.class)
    public void invalidNumberFormat() {
        cut.convert("21h");
    }
}
