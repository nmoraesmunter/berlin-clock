package com.inkglobal.techtest.converter;

import java.security.InvalidParameterException;

/**
 * User: nmoraes
 * Date: 12/04/14
 */
public class HoursConverter implements TimeUnitConverter {

    private static final int NUMBER_OF_LAMPS_PER_ROW = 4;
    private static final int HOURS_PER_LAMP = 5;

    @Override
    public String convert(String time) {
        StringBuilder berlinClockHours = new StringBuilder();
        int hours = Integer.valueOf(time);
        if (hours < 0 || hours > 24) {
            throw new InvalidParameterException("Hours time unit must be between 0 and 24");
        }

        int firstRowOn = hours / HOURS_PER_LAMP; //To calculate how many lamps are switched on on the first row
        int secondRowOn = hours % HOURS_PER_LAMP; //To calculate how many lamps are switched on on the second row

        for (int i = 0; i < NUMBER_OF_LAMPS_PER_ROW; i++) {
            if (i < firstRowOn) {
                berlinClockHours.append(RED_LAMP);
            } else {
                berlinClockHours.append(OFF_LAMP);
            }
        }
        berlinClockHours.append(LAMP_GROUP_SEPARATOR);
        for (int i = 0; i < NUMBER_OF_LAMPS_PER_ROW; i++) {
            if (i < secondRowOn) {
                berlinClockHours.append(RED_LAMP);
            } else {
                berlinClockHours.append(OFF_LAMP);
            }
        }
        return berlinClockHours.toString();
    }


}
