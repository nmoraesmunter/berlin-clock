package com.inkglobal.techtest.converter;

import java.security.InvalidParameterException;

/**
 * User: nmoraes
 * Date: 12/04/14
 */

public class MinutesConverter implements TimeUnitConverter {

    private static final int NUMBER_OF_LAMPS_FIRST_ROW = 11;
    private static final int NUMBER_OF_LAMPS_SECOND_ROW = 4;
    private static final int MINUTES_PER_LAMP = 5;

    @Override
    public String convert(String time) {
        StringBuilder berlinClockMinutes = new StringBuilder();

        int minutes = Integer.valueOf(time);
        if (minutes < 0 || minutes > 59) {
            throw new InvalidParameterException("Minutes time unit must be between 0 and 59");
        }

        int firstRowOn = minutes / MINUTES_PER_LAMP;
        int secondRowOn = minutes % MINUTES_PER_LAMP;

        for (int i = 0; i < NUMBER_OF_LAMPS_FIRST_ROW; i++) {
            if (i < firstRowOn) {
                if ((i + 1) % 3 == 0) {
                    berlinClockMinutes.append(RED_LAMP);
                } else {
                    berlinClockMinutes.append(YELLOW_LAMP);
                }
            } else {
                berlinClockMinutes.append(OFF_LAMP);
            }
        }
        berlinClockMinutes.append(LAMP_GROUP_SEPARATOR);
        for (int i = 0; i < NUMBER_OF_LAMPS_SECOND_ROW; i++) {
            if (i < secondRowOn) {
                berlinClockMinutes.append(YELLOW_LAMP);
            } else {
                berlinClockMinutes.append(OFF_LAMP);
            }
        }

        return berlinClockMinutes.toString();
    }
}
