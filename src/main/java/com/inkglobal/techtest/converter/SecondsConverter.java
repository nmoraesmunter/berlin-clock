package com.inkglobal.techtest.converter;

import java.security.InvalidParameterException;

/**
 * User: nmoraes
 * Date: 12/04/14
 */

public class SecondsConverter implements TimeUnitConverter {


    @Override
    public String convert(String time) {
        int seconds = Integer.valueOf(time);
        if (seconds < 0 || seconds > 59) {
            throw new InvalidParameterException("Seconds time unit must be between 0 and 59");
        }
        return seconds % 2 == 0 ? YELLOW_LAMP : OFF_LAMP;
    }
}
