package com.inkglobal.techtest.converter;

/**
 * User: nmoraes
 * Date: 12/04/14
 */

public interface TimeUnitConverter {

    public static final String YELLOW_LAMP = "Y";
    public static final String RED_LAMP = "R";
    public static final String OFF_LAMP = "O";
    public static final String LAMP_GROUP_SEPARATOR = "\n";


    String convert(String time);

}
