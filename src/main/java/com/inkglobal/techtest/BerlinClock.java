package com.inkglobal.techtest;

import com.inkglobal.techtest.converter.HoursConverter;
import com.inkglobal.techtest.converter.MinutesConverter;
import com.inkglobal.techtest.converter.SecondsConverter;
import com.inkglobal.techtest.converter.TimeUnitConverter;

import java.security.InvalidParameterException;

public class BerlinClock {

    private SecondsConverter secondsConverter;
    private MinutesConverter minutesConverter;
    private HoursConverter hoursConverter;

    public BerlinClock() {
        this.secondsConverter = new SecondsConverter();
        this.minutesConverter = new MinutesConverter();
        this.hoursConverter = new HoursConverter();
    }

    public String convert(String hours, String minutes, String seconds) {
        StringBuilder berlinClock = new StringBuilder();
        berlinClock.append(secondsConverter.convert(seconds));
        berlinClock.append(TimeUnitConverter.LAMP_GROUP_SEPARATOR);
        berlinClock.append(hoursConverter.convert(hours));
        berlinClock.append(TimeUnitConverter.LAMP_GROUP_SEPARATOR);
        berlinClock.append(minutesConverter.convert(minutes));
        berlinClock.append(TimeUnitConverter.LAMP_GROUP_SEPARATOR);

        return berlinClock.toString();

    }


    public static void main(String args[]) {

        if (args.length != 1) {
            throw new InvalidParameterException("Expected format of the time parameter is hh:mm:ss (without spaces)");
        }

        String[] time = args[0].split(":");

        if (time.length != 3) {
            throw new InvalidParameterException("Format of time is incorrect, it has to be hh:mm:ss");
        }

        System.out.println(new BerlinClock().convert(time[0], time[1], time[2]));

    }


}
